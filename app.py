from flask import Flask, Response, jsonify, request
import readConfigurationsFile as conf
from pymongo import MongoClient
import mongoDBController as mongo
from flask_cors import CORS
import json
import pickle

configurations = {}
conf.loadConfigurations('settings.conf', configurations)
print(configurations)
client = MongoClient('mongodb://'+str(configurations['HOST_MONGO'])+':'+str(configurations['PORT_MONGO'])+'/')
db = client[str(configurations['DATABASE_MONGO'])]

app = Flask(__name__)
CORS(app)

@app.route('/test', methods=['GET'])
def test():
    return 'Hello!'


@app.route('/next', methods=['GET'])
def next():
    intent = request.args.get('intent', type = str)

    pickle_in = open("./pm.pickle","rb")
    graph = pickle.load(pickle_in)

    if intent in graph.activities:
        value = max(graph.dependency_matrix[intent], key=graph.dependency_matrix[intent].get)
        if(value != 'NO'):
            return value
    return "None"

@app.route('/siaa', methods=['GET'])
def get_logS():
    return mongo.getLog(db)

@app.route('/prediction', methods=['GET'])
def get_logP():
    return mongo.getPrediction(db)

@app.route('/satisfaction', methods=['GET'])
def get_logR():
    return mongo.getSatisfaction(db)

@app.route('/siaa', methods=['POST'])
def add_logS():
    log=json.loads(request.data)
    print('Adding a tuple...')
    print(log)
    return jsonify({'insertedId':mongo.addLog(db, log)})

@app.route('/prediction', methods=['POST'])
def add_logP():
    prediction=json.loads(request.data)
    print('Adding a tuple...')
    print(prediction)
    return jsonify({'insertedId':mongo.addPrediction(db, prediction)})

@app.route('/satisfaction', methods=['POST'])
def add_logN():
    satisfaction=json.loads(request.data)
    print('Adding a tuple...')
    print(satisfaction)
    return jsonify({'insertedId':mongo.addSatisfaction(db, satisfaction)})

if __name__ == '__main__':
    app.run(host=str(configurations['HOST']), port=str(configurations['PORT']))
