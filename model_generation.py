import os
from pm4py.objects.log.adapters.pandas import csv_import_adapter
from pm4py.objects.conversion.log import factory as conversion_factory
from pm4py.util import constants
from pm4py.objects.log.util import sorting
from pm4py.algo.filtering.log.end_activities import end_activities_filter
from pm4py.algo.discovery.heuristics import factory as heuristics_miner
import readConfigurationsFile as conf
import pickle
import requests
import json
import pandas as pd

def main():
    configurations = {}
    conf.loadConfigurations('settings.conf', configurations)
    print(configurations)
    print(str(configurations['URL']))

    response = requests.get(str(configurations['URL']))
    dataframe = pd.DataFrame.from_dict(response.json())
    dataframe = dataframe.rename(columns={'Intent': 'concept:name', 'User': 'case:concept:name', 'Timestamp': 'time:timestamp'})

    month=dataframe['Month']==str(configurations['MONTH'])
    dataframe=dataframe[month]

    log = conversion_factory.apply(dataframe, parameters={constants.PARAMETER_CONSTANT_CASEID_KEY: "case:concept:name",
                                                        constants.PARAMETER_CONSTANT_ACTIVITY_KEY: "concept:name",
                                                        constants.PARAMETER_CONSTANT_TIMESTAMP_KEY: "time:timestamp"})
    log = sorting.sort_timestamp(log)
    end_activities = end_activities_filter.get_end_activities(log)
    filtered_log = end_activities_filter.apply(log, ["NO"])

    heu_net = heuristics_miner.apply_heu(log)

    pickle_out = open("pm.pickle","wb")
    pickle.dump(heu_net, pickle_out)
    pickle_out.close()



if __name__ == '__main__':
    main()