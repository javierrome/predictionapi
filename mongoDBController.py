from bson.json_util import dumps
from bson.objectid import ObjectId	

def getLog(db):
    log=db.log
    return dumps(list(log.find()))

def getPrediction(db):
    pred=db.prediction
    return dumps(list(pred.find()))

def getSatisfaction(db):
    points=db.punctuation
    return dumps(list(points.find()))


def addLog(db, log):
    logg=db.log
    log_data={
        'Intent': log['Intent'],
	    'User': log['User'],
        'Dialog': log['Dialog'],
        'Timestamp': log['Timestamp'],
        'Date': log['Date'],
        'Hour': log['Hour'],
        'Month': log['Month'],
        'Channel': log['Channel']
    }
    return str(logg.insert_one(log_data).inserted_id)

def addSatisfaction(db, punctuation):
    points=db.punctuation
    punctuation_data={
        'Punctuation': log['Punctuation'],
	    'User': log['User'],
        'Timestamp': log['Timestamp'],
        'Date': log['Date'],
        'Hour': log['Hour'],
        'Month': log['Month'],
        'Channel': log['Channel']
    }
    return str(points.insert_one(punctuation_data).inserted_id)


def addPrediction(db, prediction):
    pred=db.prediction
    prediction_data={
        'Intent': log['Intent'],
	    'User': log['User'],
        'Dialog': log['Dialog'],
        'Timestamp': log['Timestamp'],
        'Date': log['Date'],
        'Hour': log['Hour'],
        'Month': log['Month'],
        'Channel': log['Channel']
    }
    return str(pred.insert_one(prediction_data).inserted_id)


def deleteCollection(db):
    db.covid19.drop()
